# Testing a mailserver

## Basic testing with swaks

1. Make sure that swaks is installed:

   ```
   apt install swaks
   ```

2. Try to send a message from `postmaster` to an external email
   address:

   ```
   source settings.sh
   swaks --server $HOSTNAME \
         --from postmaster@$MAILDOMAIN \
         --to xyz@gmail.com \
         --auth-user postmaster@$MAILDOMAIN \
         --auth-password $POSTMASTER_PASSWD
   tail logs/mail.log
   ```
   
   Check the spam folder of your Gmail account.
   
3. Reply to that message from the Gmail account and check that you got
   the reply on the server:
   
   ```
   tail logs/mail.log
   ls data/$MAILDOMAIN/postmaster/
   cat data/$MAILDOMAIN/postmaster/new/*
   ```
   
4. Create another email account and test this one too:

   ```
   ./setup.sh email list
   ./setup.sh email add admin@$MAILDOMAIN pass123 
   ./setup.sh email list
   cat config/postfix-accounts.cf
   ```
   
   ```
   swaks -s $HOSTNAME \
         -f postmaster@$MAILDOMAIN \
		 -t admin@MAILDOMAIN \
		 -au postmaster@$MAILDOMAIN \
		 -ap $POSTMASTER_PASSWD
   tail logs/mail.log
   ls data/$MAILDOMAIN/admin/
   cat data/$MAILDOMAIN/admin/new/*
   ```

   ```
   swaks -s mail.example.org \
         -f admin@example.org \
		 -t postmaster@example.org \
		 -au admin@example.org \
		 -ap pass123
   tail logs/mail.log
   ls data/example.org/postmaster/
   cat data/example.org/postmaster/new/*
   ```

5. Create an email alias (forwarder) and test it:

   ```
   ./setup.sh alias add info@example.org xyz@gmail.com
   ./setup.sh alias list
   
   swaks -s mail.example.org \
         -f admin@example.org \
		 -t info@example.org \
		 -au admin@example.org \
		 -ap pass123
   tail logs/mail.log
   ```
   
   Check the spam folder of the Gmail account.

## Testing with mutt

1. Make sure that `mutt` is installed:

   ```
   apt install mutt
   ```

2. Create a `mutt` configuration for the account
   `postmaster@example.org`, with a content like this:

   ```
   cat << EOF > postmaster.mutrc
   # Sending
   set from = "postmaster@example.org"
   set realname = "Postmaster"
   set smtp_url = "smtp://postmaster@example.org@mail.example.org:587/"
   set smtp_pass = "pass123"

   # Receiving
   set imap_user = "postmaster@example.org"
   set imap_pass = "pass123"
   set folder = "imap://mail.example.org:143"
   set spoolfile = "+INBOX"

   # Where to put the stuff
   set header_cache = ".mutt1/cache/headers"
   set message_cachedir = ".mutt1/cache/bodies"
   set certificate_file = ".mutt1/certificates"

   EOF
   ```

3. Create another one for the account `admin@example.org`:

   ```
   cat << EOF > admin.mutrc
   # Sending
   set from = "admin@example.org"
   set realname = "Admin"
   set smtp_url = "smtp://admin@example.org@mail.example.org:587/"
   set smtp_pass = "pass123"

   # Receiving
   set imap_user = "admin@example.org"
   set imap_pass = "pass123"
   set folder = "imap://mail.example.org:143"
   set spoolfile = "+INBOX"

   # Where to put the stuff
   set header_cache = ".mutt2/cache/headers"
   set message_cachedir = ".mutt2/cache/bodies"
   set certificate_file = ".mutt2/certificates"

   EOF
   ```

4. Start `mutt` with one of these config files, like this:

   ```
   mutt -F postmaster.muttrc
   mutt -F admin.muttrc
   ```
   
   
## Test multiple email domains

To create another email domain, you just need to create email accounts
on this domain, using `./setup.sh`. Like this:

```
./setup.sh email add admin@test1.example.org pass123
./setup.sh email list
./setup.sh alias add info@test1.example.org xyz@gmail.com
./setup.sh alias list
```

**Note:** Make sure to add something like this in the DNS records:

```
test1.example.org.    IN    MX    10    mail.example.org.
test1.example.org.    IN    TXT         "v=spf1 mx -all"
```

Then try to send emails from and to the other addresses:

```
swaks -s mail.example.org \
	  -f admin@example.org \
	  -t admin@test1.example.org \
	  -au admin@example.org \
	  -ap pass123
tail logs/mail.log
ls data/test1.example.org/admin/
cat data/test1.example.org/admin/new/*

swaks -s mail.example.org \
      -f admin@test1.example.org \
      -t admin@example.org \
      -au admin@test1.example.org \
      -ap pass123
tail logs/mail.log
ls data/example.org/admin/
cat data/example.org/admin/new/*
```
