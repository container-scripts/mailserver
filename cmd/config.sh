cmd_config() {
    cs inject debian-fixes.sh

    cs @wsproxy get-ssl-cert postmaster@$MAILDOMAIN $HOSTNAME
    cs copy-ssl-cert
    _create_cron_job_to_copy_ssl_cert
}


# create a cron job that copies the ssl cert from wsproxy once a week
_create_cron_job_to_copy_ssl_cert() {
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  cs @$dir copy-ssl-cert &> /dev/null
EOF
}
