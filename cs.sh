#!/bin/bash

cmd_start() {
    podman pod start $MAILDOMAIN
}

cmd_stop() {
    podman pod stop $MAILDOMAIN
}

cmd_restart() {
    podman pod restart $MAILDOMAIN
}

cmd_remove() {
    podman pod rm -f $MAILDOMAIN
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
}

cmd_make() {
    # customize mailserver.env
    cs set OVERRIDE_HOSTNAME $HOSTNAME
    cs set ONE_DIR 1
    cs set POSTMASTER_ADDRESS postmaster@$MAILDOMAIN
    cs set SPOOF_PROTECTION 1
    cs set ENABLE_SRS 1
    cs set ENABLE_FAIL2BAN 1
    cs set SSL_TYPE letsencrypt
    cs set POSTFIX_INET_PROTOCOLS ipv4
    
    # create container-compose.yaml
    cat <<EOF > container-compose.yaml
version: '3.8'

networks:
  cs:
    external:
      name: $NETWORK

services:
  mail:
    container_name: $CONTAINER
    image: $IMAGE
    restart: unless-stopped
    cap_add:
      - NET_ADMIN
    volumes:
      - ./data:/var/mail
      - ./state:/var/mail-state
      - ./logs:/var/log/mail
      - ./config/:/tmp/docker-mailserver/
      - ./sslcert/:/etc/letsencrypt/live/$HOSTNAME/
      - ./:/host/
    env_file:
      - mailserver.env
    ports:
      - "25:25"
      - "465:465"
      - "587:587"
      - "143:143"
      - "993:993"
      - "110:110"
      - "995:995"
      - "4190:4190"
    hostname: $HOSTNAME
    domainname: $MAILDOMAIN
    networks:
      cs:
        aliases:
          - $HOSTNAME
EOF

    # start the container
    mkdir -p logs data state config sslcert
    podman-compose -p $MAILDOMAIN up -d

    # create an email account for postmaster
    cs setup email add postmaster@$MAILDOMAIN $POSTMASTER_PASSWD
    
    # make some configurations
    cs config
}
