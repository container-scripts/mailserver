APP=mailserver

MAILDOMAIN="example.org"
HOSTNAME="mail.example.org"

# Container settings
IMAGE=tvial/docker-mailserver:stable
CONTAINER=$HOSTNAME

# Password for postmaster@$MAILDOMAIN
POSTMASTER_PASSWD="pass123"
