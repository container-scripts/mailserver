### run custom init steps

# make sure that podman-compose is installed
if ! hash podman-compose 2>/dev/null; then
    curl -o /usr/local/bin/podman-compose \
	 https://raw.githubusercontent.com/containers/podman-compose/devel/podman_compose.py
    chmod +x /usr/local/bin/podman-compose
fi

# get config files and scripts from 'tomav/docker-mailserver'
wget -q https://raw.githubusercontent.com/tomav/docker-mailserver/master/mailserver.env
wget -q https://raw.githubusercontent.com/tomav/docker-mailserver/master/setup.sh
sed -i setup.sh \
    -e '/command -v docker/,/CRI=docker/ d' \
    -e '/command -v podman/ s/elif/if/'
chmod a+x setup.sh
